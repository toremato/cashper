// var gulp        = require('gulp');
// var sass        = require('gulp-sass');
// var browserSync = require('browser-sync').create();
//
//
// gulp.task('sass', function () {
//     return gulp.src('./public/stylesheets/*.scss')
//         .pipe(sass({
//             outputStyle: 'compressed'
//         }).on('error', sass.logError))
//         .pipe(gulp.dest('./public/stylesheets'))
//         .pipe(browserSync.stream());
// });
//
// gulp.task('serve', function() {
//
//     browserSync.init({
//         proxy: 'http://localhost:8000',
//         logLevel: 'debug',
//         cors: true
//     });
//
//     gulp.watch("./public/stylesheets/*.scss").on("change", 'sass');
//     gulp.watch("./public/*.html").on('change', browserSync.reload);
//     gulp.watch("./public/javascripts/*.js").on('change', browserSync.reload);
//     gulp.watch("./public/stylesheets/*.css").on('change', browserSync.reload);
// });
//
// gulp.task('default', ['serve']);


var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');
var imagemin = require('gulp-imagemin');
var image = require('gulp-image');

// Static Server + watching scss/html files
gulp.task('serve', ['sass'], function() {

    browserSync.init({
        proxy: "http://localhost:3000"
    });

    gulp.watch("public/stylesheets/*.scss", ['sass', browserSync.reload]);
    gulp.watch("public/*.html").on('change', browserSync.reload);
    gulp.watch("public/stylesheets/*.css", browserSync.reload);
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src("app/scss/*.scss")
        .pipe(sass({
            outputStyle: 'expanded'
        }))
        .pipe(gulp.dest("app/css"))
        .pipe(browserSync.stream());
});

gulp.task('default', ['serve']);


gulp.task('default-2', function() {
    gulp.src('./public/images/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('public/assets/images'))
});


gulp.task('image', function () {
    gulp.src('./public/**/*')
        .pipe(image({
            optipng: true,
            jpegRecompress: true,
            guetzli: true
        }))
        .pipe(gulp.dest('./public'));
});

